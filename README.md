# 10y Celebration

Configuration & code snippets to learn and celebrate [10 years GitLab](https://about.gitlab.com/ten/). 

## CI/CD Configuration Examples 

- [conditional_jobs_rules.yml](conditional_jobs_rules.yml) runs a job only if `GITLAB_YEARS` is set to `10`. You can copy the configuration into `.gitlab-ci.yml` and manually run a pipeline in `CI/CD > Pipelines`. Add the variable and values and inspect the pipeline.
- [matrix_builds.yml](matrix_builds.yml) provides stages, values, and all of GitLab.
- [variables_in_variables.yml](variables_in_variables.yml) shows a GitLab 14.3 to re-use variables in other variables. 

## Projects

- [CI/CD Tanuki in C++](https://gitlab.com/gitlab-de/cicd-tanuki-cpp) - learn how to use a builder image in the Docker container registry, and calculate the party times for 10 years GitLab.
